#Memo Touch Startup
###Apexmic 大容量NFC SDK的基本使用范例，支持WiFi与手机NFC读写卡功能。
![Screenshots](screenshot/Screenshot_001.jpg)

##一.卡片监听
###1.获取`Device`实例，并且初始化
```java
Device mDevice= DriverFactory.getDevice(Device.TYPE_NFC); //若是通过读卡器读写时获取TYPE_WIFI
mDevice.initContext(context);
```

###2.设置卡片监听器

```java
mDevice.addCardListener(new CardListener {
        @Override
        public void onCardChanged(CardEvent cardChangedEvent) {}

        @Override
        public void onCardNotApplicable(CardEvent cardChangedEvent) {}

        @Override
        public void onCardTimeout(CardEvent cardChangedEvent) {}

        @Override
        public void onCardAttached(CardEvent cardEvent) {}

        @Override
        public void onCardUnattached(CardEvent cardEvent) {}

        @Override
        public void onCardException(CardEvent cardEvent) {});

```

`CardListener`监听卡片变更事件，包括卡片贴近、移开、卡片锁定后重新贴近，卡片移开超出设定的时间范围等。

##二.读卡

###1.设置模式

```java
mDevice.setMode(Device.MODE_READ_ONLY);
```

###2.读卡
####a.卡片贴近读卡设备时，通过卡片监听器`CardEvent`获取到卡片的基本信息,`Card` 对象可获取卡类型，名称，ID,文件列表等。

```java
    @Override
        public void onCardChanged(CardEvent cardChangedEvent) {
            Card card = cardChangedEvent.getNewCard();
            List<CardFile> fileList = card.listFile();
        }
```
####b.获取读卡对象`CardFileReader`，设置文件读取监听器及读取顺序

```java
CardFileReader reader = mDevice.getCardFileReader(card, "");
reader.setCardFileReadListener(new CardReadListenerImpl());
reader.read(fileList);
```

文件的读取进度可由`CardFileReadListener`回调事件获得：

```java
private class CardReadListenerImpl implements CardFileReadListener {
    @Override
    public void onReadStart(CardFileReadEvent cardReadEvent) {}  //文件开始读

    @Override
    public void onReadProgress(CardFileReadEvent cardReadEvent) {} //文件进度更新

    @Override
    public void onReadFinish(CardFileReadEvent cardReadEvent) {}  //文件读取完成

    @Override
    public void onReadPause(CardFileReadEvent cardReadEvent) {}   //文件读取暂停

    @Override
    public void onReadResume(CardFileReadEvent cardReadEvent) {}  //文件继续读取

    @Override
    public void onReadCancel(CardFileReadEvent cardReadEvent) {}  //文件取消读取

}
```

##三.写卡
###1.设置模式

```java
mDevice.setMode(Device.MODE_WRITE_ONLY);
```

###2. 写卡
卡片贴近读卡设备时，通过CardEvent获取代表本次写卡的`Card` 对象,

```java
    @Override
        public void onCardChanged(CardEvent cardChangedEvent) {
            Card card = cardChangedEvent.getNewCard();
        }
```

`Card`信息设置与写入文件：

```java
CardFileWriter writer = mDevice.getCardFileWriter(card, "");
card.setName(cardName);
try {
    writer.write(getWriteFiles(), writeListener, false);
} catch (PackFormatException e) {
    e.printStackTrace();
} catch (ModeException e) {
    e.printStackTrace();
}
```

文件的写入状态可由`CardFileWriteListener`回调事件获得：

```java
class CardFileWriteListenerImpl implements CardFileWriteListener {
        @Override
        public void onWriteStart(CardFileWriteEvent event) {} // 开始写卡

        @Override
        public void onWriteProgress(CardFileWriteEvent event) {}   // 进度更新

        @Override
        public void onWriteFinish(CardFileWriteEvent event) {} // 写卡完成

        @Override
        public void onWritePause(CardFileWriteEvent event) {}  // 写卡暂停（卡片拿开）

        @Override
        public void onWriteResume(CardFileWriteEvent event) {} // 继续写卡

        @Override
        public void onWriteCancel(CardFileWriteEvent event) {} // 取消写卡
    }
```

##四.使用配置

###1.加载驱动
使用SDK 前必需先加载驱动

```java
Class.forName("com.apex.iot.card.driver.MainDriverLoader");
```

###2.NFC 事件拦载与中转
当NFC 贴近手机时，系统会调用`onNewIntent()` 方法，并将NFC 相关的信息封装于`Intent` 中，APP拦截到NFC事件后交由底层处理：

```java

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction())) {
            ((NFCDevice) mDevice).setIntent(getIntent());
        super.onResume();
    }

```

###3.读卡器设置
`Device`初始化之后我们就可以使用它来建立WiFi 连接了，WiFi 连接过程的代码如下：

```java
        AdapterDevice adevice = (AdapterDevice) mDevice;
        adevice.setAdapterListener(adapterScanListener);
        try {
            adevice.scanAdapter();
        } catch (AdapterException e) {
            e.printStackTrace();
        }

```
WiFi 事件监听

```java

    private AdapterListener adapterScanListener = new AdapterListener() {

        @Override
        public void onScanResult(List<Adapter> list) {} // 扫描到的读卡器list

        @Override
        public void onConnected(AdapterEvent adapterEvent) {}  //设置读卡器WiFi信号后连接成功

        @Override
        public void onDisconnect(AdapterEvent adapterEvent) {} //设置读卡器WiFi信号后连接成功后断开

        @Override
        public void onException(AdapterEvent adapterEvent) {} //设置读卡器WiFi信号后连接不成功
    };

```

从`onScanResult(List<Adapter> list)` 回调的`list`中选择你的读卡器WiFi 信号设置给`Device`, 这样SDK就能帮你连接读卡器。

```java

    try {
        device.setAdapter(adapter);
    } catch (AdapterException e) {
        e.printStackTrace();
    }

```

##五.其它配置
###1.在AnroidManifest.xml文件中需要设置的部分有：
####a.设置权限：

```xml
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.NFC" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
```

####b.设置`Activity`的Intent Filter：

```xml
<intent-filter>
    <action android:name="android.nfc.action.TECH_DISCOVERED" />
</intent-filter>
```

####c.限制Android版本, Android Studio 在build.gradle文件中配置
`android:minSdkVersion="14"`

<center>Copyright ©2016 珠海艾派克科技股份有限公司</center>