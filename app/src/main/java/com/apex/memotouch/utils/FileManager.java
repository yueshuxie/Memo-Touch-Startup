package com.apex.memotouch.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 文件管理类
 * <p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 * Created by Simon.
 */
public class FileManager {


    public static void deleteFile(String filePath) {
        if (filePath == null) return;
        File file = new File(filePath);

        if (!file.exists()) return;
        if (file.isFile()) {
            file.delete();
            return;
        }
        if (file.isDirectory()) {
            File[] childFile = file.listFiles();
            if (childFile == null || childFile.length == 0) {
                file.delete();
                return;
            }
            for (File f : childFile) {
                deleteFile(f.getAbsolutePath());
            }
            file.delete();
        }
    }

    public static String generateFolder(String folderPath) {
        File parent = new File(folderPath);
        if (!parent.exists()) {
            System.out.println(parent.mkdirs());
        }
        return parent.getAbsolutePath();
    }

    public static String copyRaw(int rawId, String sdCardFilepath,Context context) {
        InputStream in = context.getResources().openRawResource(rawId);
        try {
            File file = new File(sdCardFilepath);
            if (file.exists()) return sdCardFilepath;
            OutputStream out = new FileOutputStream(new File(sdCardFilepath));
            writeFile(in, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return sdCardFilepath;
    }


    /**
     * 获取app名字
     *
     * @return 返回app名字
     */
    public static String getApplicationName(Context context) {
        PackageManager packageManager = null;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = context.getApplicationContext().getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        String applicationName =
                (String) packageManager.getApplicationLabel(applicationInfo);
        return applicationName;
    }

    private static void writeFile(InputStream in, OutputStream out) {
        try {
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
