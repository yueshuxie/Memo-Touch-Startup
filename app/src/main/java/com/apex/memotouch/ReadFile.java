package com.apex.memotouch;

/**
 *<p>Copyright © 2016年 珠海艾派克科技股份有限公司. All rights reserved.
 *
 */
public class ReadFile {

    private String path;
    private float progress;
    private long length;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }
}
